<?php
require ('animal.php');
require ('frog.php');
require ('Ape.php');


$sheep = new Animal("shaun");

$kodok = new Frog("buduk");
$kodok->jump() ; // "hop hop"

$sungokong = new Ape("kera sakti");
$sungokong->yell(); // "Auooo"


echo "Nama :".$sheep->name; // "shaun"
echo "<br>";
echo "Legs :".$sheep->legs; // 2
echo "<br>";
echo "Cold Blooded :".$sheep->cold_blooded ;// false
echo "<br>";
// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
echo "<br>nama :".$kodok->name;
echo "<br>";
echo "Legs :".$kodok->legs;
echo "<br>";
echo "Cold Blooded :".$kodok->coold_blooded ;// false
echo "<br>";
echo "jump :".$kodok->jump();
echo "<br>";
echo "<br>nama :" .$sungokong->name;
echo "<br>";
echo "Legs :".$sungokong->legs;
echo "<br>";
echo "Cold Blooded :".$sungokong->coold_blooded ;// false
echo "<br>";
echo "yell :" .$sungokong->yell();


?>