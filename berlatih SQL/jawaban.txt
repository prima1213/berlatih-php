
Jawaban 1 dan 2 :

-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 05, 2020 at 03:30 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `myshop`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`) VALUES
(1, 'gadget'),
(2, 'cloth'),
(3, 'men'),
(4, 'women'),
(5, 'branded');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `passwrd` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `passwrd`) VALUES
(1, 'John Doe', 'john@doe.com', 'john123'),
(2, 'jane Doe', 'jane@doe.com', 'jenita123');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `items`
--
ALTER TABLE `items`
  ADD CONSTRAINT `items_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


jawaban 3 : INSERT INTO `users` (`id`, `name`, `email`, `passwrd`) 
				VALUES (NULL, 'John Doe', 'john@doe.com', 'john123'), (NULL, 'jane Doe', 'jane@doe.com', 'jenita123');

			INSERT INTO `categories` (`id`, `name`) 
				VALUES (NULL, 'gadget'), (NULL, 'cloth'), (NULL, 'men'), (NULL, 'women'), (NULL, 'branded');
				
			INSERT INTO `items` (`id`, `name`, `description`, `price`, `stock`, `category_id`) 
				VALUES (NULL, 'Sumsang b50'), (NULL, 'hape keren dari merek sumsang'), (NULL, '4000000'), (NULL, '100'), (NULL, '1');
				
			INSERT INTO `items` (`id`, `name`, `description`, `price`, `stock`, `category_id`) 
				VALUES (NULL, 'Uniklooh'), (NULL, 'baju keren dari brand tername'), (NULL, '500000'), (NULL, '50'), (NULL, '2');
			
			INSERT INTO `items` (`id`, `name`, `description`, `price`, `stock`, `category_id`) 
				VALUES (NULL, 'IMHO Watch'), (NULL, 'jam tangan anak yang jujur banget'), (NULL, '2000000'), (NULL, '10'), (NULL, '1');

jawaban no 4 a : SELECT `id`, `name`, `email` FROM `users` 

jawaban no 4 b : SELECT `id`, `name`, `description`, `price`, `stock`, `category_id` FROM `items` WHERE `price`>1000000 

jawaban no 4 b : SELECT * FROM `items` WHERE `name` LIKE '%watch%' 

jawaban no 5 : UPDATE `items` SET `price` = '2500000' WHERE `items`.`id` = 1; 

